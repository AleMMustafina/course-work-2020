#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
# import StringIO
import time
from io import BytesIO
from random import choice, randint, random

# import numpy as np
# import tensorflow as tf
import xlwt as xlwt
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.utils.translation import ugettext
from pymystem3 import Mystem

import xlsxwriter
from generator.models import CRITERIA_CHOICES, Discipline, Skill

from .forms import GenerateTableForm, UploadFileForm

import re
import gensim
import pymorphy2

model = gensim.models.KeyedVectors.load_word2vec_format("model.bin",
                                                        binary=True)
model.init_sims(replace=True)

cash_neighb = {}

morph = pymorphy2.MorphAnalyzer()
punct = re.compile('^(.*?)([а-яА-ЯёЁ-]+)(.*?)$')
capit = re.compile('^[А-Я]+$')

cotags = {'ADJF':'ADJ', # pymorphy2: word2vec
'ADJS' : 'ADJ',
'ADVB' : 'ADV',
'COMP' : 'ADV',
'GRND' : 'VERB',
'INFN' : 'VERB',
'NOUN' : 'NOUN',
'PRED' : 'ADV',
'PRTF' : 'ADJ',
'PRTS' : 'VERB',
'VERB' : 'VERB'}

capit_letters = [chr(x) for x in range(1040,1072)] + ['Ё']

def search_neighbour(word, pos, gend='masc'):
    word = word.replace('ё', 'е')
    lex = word + '_' + cotags[pos]
    if lex in model:
        neighbs = model.most_similar([lex], topn=20)
        for nei in neighbs:
            lex_n, ps_n = nei[0].split('_')
            if '::' in lex_n:
                continue
            if cotags[pos] == ps_n:
                if pos == 'NOUN':
                    parse_result = morph.parse(lex_n)
                    for ana in parse_result:
                        if ana.normal_form == lex_n:
                            if ana.tag.gender == gend:
                                return lex_n
                elif cotags[pos] == 'VERB' and word[-2:] == 'ся':
                    if lex_n[-2:] == 'ся':
                        return lex_n
                elif cotags[pos] == 'VERB' and word[-2:] != 'ся':
                    if lex_n[-2:] != 'ся':
                        return lex_n
                else:
                    return lex_n
    return None

def flection(lex_neighb, tags):
    tags = str(tags)
    tags = re.sub(',[AGQSPMa-z-]+? ', ',', tags)
    tags = tags.replace("impf,", "")
    tags = re.sub('([A-Z]) (plur|masc|femn|neut|inan)', '\\1,\\2', tags)
    tags = tags.replace("Impe neut", "")
    tags = tags.split(',')
    tags_clean = []
    for t in tags:
        if t:
            if ' ' in t:
                t1, t2 = t.split(' ')
                t = t2
            tags_clean.append(t)
    tags = frozenset(tags_clean)
    prep_for_gen = morph.parse(lex_neighb)
    ana_array = []
    for ana in prep_for_gen:
        if ana.normal_form == lex_neighb:
            ana_array.append(ana)
    for ana in ana_array:
        try:
            flect = ana.inflect(tags)
        except:
            print(tags)
            return None
        if flect:
            word_to_replace = flect.word
            return word_to_replace
    return None


def rephrase(gen):
    rephrased_text = ''
    text, first_word_cnt = gen
    new_line = []
    line = text.strip()
    words = line.split(' ')
    words[0] = words[0].title()
    new_line.extend(words[0:first_word_cnt])
    for word in words[first_word_cnt:]:
        struct = punct.findall(word)
        if struct:
            struct = struct[0]
        else:
            new_line.append(word)
            continue
        # print (struct)
        wordform = struct[1]
        if wordform:
            if capit.search(wordform):
                new_line.append(word)
                continue
            else:
                if wordform[0] in capit_letters:
                    capit_flag = 1
                else:
                    capit_flag = 0
            parse_result = morph.parse(wordform)[0]
            if 'Name' in parse_result.tag or 'Patr' in parse_result.tag:
                new_line.append(word)
                continue
            if parse_result.normal_form == 'глава':
                new_line.append(word)
                continue
            pos_flag = 0
            for tg in cotags:
                if tg in parse_result.tag:
                    pos_flag = 1
                    lex = parse_result.normal_form
                    pos_tag = parse_result.tag.POS
                    if (lex, pos_tag) in cash_neighb:
                        lex_neighb = cash_neighb[(lex, pos_tag)]
                    else:
                        if pos_tag == 'NOUN':
                            gen_tag = parse_result.tag.gender
                            lex_neighb = search_neighbour(lex, pos_tag,
                                                          gend=gen_tag)
                        else:
                            lex_neighb = search_neighbour(lex, pos_tag)
                        cash_neighb[(lex, pos_tag)] = lex_neighb
                    if not lex_neighb:
                        new_line.append(word)
                        break
                    else:
                        if pos_tag == 'NOUN':
                            if parse_result.tag.case == 'nomn' and parse_result.tag.number == 'sing':
                                if capit_flag == 1:
                                    lex_neighb = lex_neighb.capitalize()
                                new_line.append(
                                    struct[0] + lex_neighb + struct[2])
                            else:
                                word_to_replace = flection(lex_neighb,
                                                           parse_result.tag)
                                if word_to_replace:
                                    if capit_flag == 1:
                                        word_to_replace = word_to_replace.capitalize()
                                    new_line.append(
                                        struct[0] + word_to_replace +
                                        struct[2])
                                else:
                                    new_line.append(word)

                        elif pos_tag == 'ADJF':
                            if parse_result.tag.case == 'nomn' and parse_result.tag.number == 'sing':
                                if capit_flag == 1:
                                    lex_neighb = lex_neighb.capitalize()
                                new_line.append(
                                    struct[0] + lex_neighb + struct[2])
                            else:
                                word_to_replace = flection(lex_neighb,
                                                           parse_result.tag)
                                if word_to_replace:
                                    if capit_flag == 1:
                                        word_to_replace = word_to_replace.capitalize()
                                    new_line.append(
                                        struct[0] + word_to_replace +
                                        struct[2])
                                else:
                                    new_line.append(word)

                        elif pos_tag == 'INFN':
                            if capit_flag == 1:
                                lex_neighb = lex_neighb.capitalize()
                            new_line.append(
                                struct[0] + lex_neighb + struct[2])

                        elif pos_tag in ['ADVB', 'COMP', 'PRED']:
                            if capit_flag == 1:
                                lex_neighb = lex_neighb.capitalize()
                            new_line.append(
                                struct[0] + lex_neighb + struct[2])

                        else:
                            word_to_replace = flection(lex_neighb,
                                                       parse_result.tag)
                            if word_to_replace:
                                if capit_flag == 1:
                                    word_to_replace = word_to_replace.capitalize()
                                new_line.append(
                                    struct[0] + word_to_replace +
                                    struct[2])
                            else:
                                new_line.append(word)
                    break
            if pos_flag == 0:
                new_line.append(word)
        else:
            new_line.append(''.join(struct))

        # new_line[0] = new_line[0].title()

    return " ".join(new_line)


def tag_mystem(text):
    m = Mystem()
    raw = text.split(' ')
    processed = m.analyze(text)
    tagged = []
    for w in processed:
        try:
            lemma = w["analysis"][0]["lex"].lower().strip()
            pos = w["analysis"][0]["gr"].split(',')[0]
            pos = pos.split('=')[0].strip()
            tagged.append(lemma.lower() + '_' + pos)
        except KeyError:
            continue
    return tagged, raw


def load_keywords():
    keywords = []
    kw_file = open("keywords.txt", encoding="utf-8")
    index = 0
    for l in kw_file:
        if index % 6 != 0 and index % 6 != 5:
            keywords.append(l.replace('\n', '').split(','))
        index += 1

    return keywords


def generate(data):
    tagged, raw = data
    key = 0  # Добавляем к индексу в зависимости от ключевого слова
    keywords = load_keywords()

    # Первое слово в lowercase
    raw[0] = raw[0].lower()

    gens = []

    # Поиск ключевых слов, к которым можно прикрепить оценки
    for i in range(4):
        local_raw = raw.copy()
        for w in tagged:
            index = tagged.index(w)  # Сохранить индекс слова
            if "знать" in w:
                key = 0
            elif "уметь" in w:
                key = 4
            elif "владеть" in w:
                key = 8
            elif "навык" in w:
                key = 12
            else:  # Не вставляем слова
                index = -1

            if index >= 0:
                word = choice(keywords[i + key])
                if word == "не имеет навыков":  # Костыль
                    local_raw[index - 1] = "не имеет"
                    local_raw[index] = "навыков"
                else:
                    local_raw[index] = "{} {}".format(word, local_raw[index])

        gens.append(
            ((" ".join(local_raw)), len(word))
        )

    return gens


def write_to_excel(skills, town=None):
    output = BytesIO()
    workbook = xlsxwriter.Workbook(output)

    worksheet_s = workbook.add_worksheet("Лист 1")
    worksheet_s.set_column('A:E', 30)
    worksheet_s.set_row(0, 25)
    worksheet_s.set_row(1, 25)

    mark_header = workbook.add_format({
        'align': 'center',
        'valign': 'top',
        'border': 1,
        'font_size': 12,
        'bold': True,
        'font_name': 'Times New Roman'
    })
    mark_header.set_text_wrap()

    criteria_header = workbook.add_format({
        'align': 'center',
        'valign': 'top',
        'border': 1,
        'font_size': 12,
        'bold': True,
        'font_name': 'Times New Roman'

    })
    criteria_header.set_text_wrap()

    wrap_format = workbook.add_format({
        'align': 'center',
        'valign': 'top',
        'border': 1,
        'font_size': 12,
        'font_name': 'Times New Roman'
    })
    wrap_format.set_text_wrap()

    # wrap_format.set_align('center')
    # wrap_format.set_align('vcenter')

    worksheet_s.merge_range('A1:A2',
                            "Планируемые\nрезультаты\nобучения",
                            mark_header
                            )
    worksheet_s.merge_range('B1:E1',
                            'Критерии оценивания результатов обучения',
                            criteria_header)

    worksheet_s.write(1, 1, "Неудовлетворительно", mark_header)
    worksheet_s.write(1, 2, 'Удовлетворительно', mark_header)
    worksheet_s.write(1, 3, 'Хорошо', mark_header)
    worksheet_s.write(1, 4, 'Отлично', mark_header)

    # worksheet_s.write(4, 3, ugettext(u"Max T. (℃)"), header)
    # the rest of the headers from the HTML file

    for idx, data in enumerate(skills):
        row = 2 + idx
        gens = generate(tag_mystem(data.name))

        worksheet_s.write_string(row, 0,
                                 data.code + ' ' + data.name,
                                 wrap_format)
        worksheet_s.write_string(row, 1, rephrase(gens[0]), wrap_format)
        worksheet_s.write_string(row, 2, rephrase(gens[1]), wrap_format)
        worksheet_s.write_string(row, 3, rephrase(gens[2]), wrap_format)
        worksheet_s.write_string(row, 4, rephrase(gens[3]), wrap_format)

    workbook.close()
    xlsx_data = output.getvalue()

    return xlsx_data


def generate_table(disc):
    # Получение компетенций для дисциплины
    skills = Skill.objects.filter(disc=disc)

    # # Заголовок таблицы
    # html = '<table border="1">'
    # html += "<tr>"
    # html += '<th rowspan="2">{}</th>'.format("Планируемые результаты обучения")
    # html += '<th colspan="4">{}</th>'.format("Критерии оценивания результатов обучения")
    # html += "</tr>"
    #
    # html += "<tr>"
    # for i in range(4):
    #     html += '<td>{}</td>'.format(CRITERIA_CHOICES[3 - i][1])
    # html += "</tr>"

    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = \
        'attachment; filename=Tablitsa_sootvetsviy_planiruyemykh_rezultatov_kriteriyam_obucheniya.xlsx'

    #     response = HttpResponse(content_type='application/ms-excel')
    #     response['Content-Disposition'] = 'attachment; filename="users.xls"'
    #
    xlsx_data = write_to_excel(skills)
    response.write(xlsx_data)
    #     wb = xlwt.Workbook(encoding='utf-8')
    #     ws = wb.add_sheet('Users')
    #     # Sheet header, first row
    #     row_num = 0
    #     font_style = xlwt.XFStyle()
    #     font_style.font.bold = True
    #
    #
    #     columns = ['Username', 'First name', 'Last name', 'Email address', ]
    #     for col_num in range(len(columns)):
    #         ws.write(row_num, col_num, columns[col_num], font_style)
    # # Sheet body, remaining rows
    #     font_style = xlwt.XFStyle()
    #     rows = Discipline.objects.all().values_list('name', 'id')
    #     for row in rows:
    #         row_num += 1
    #         for col_num in range(len(row)):
    #             ws.write(row_num, col_num, row[col_num], font_style)
    #     wb.save(response)

    # Заполнение таблицы
    #     for skill in skills:
    #         descriptions = generate(tag_mystem(skill.name))
    #         html += "<tr>"
    #         for i in range(5):
    #             if i == 0:
    #                 html += '<td>{}. {}</td>'.format(skill.code, skill.name)
    #             else:
    #                 html += '<td>{}</td>'.format(descriptions[i - 1].capitalize())
    #         html += "</tr>"
    #
    #     html += "</table>"

    # return html
    return response


def handle_uploaded_file(f, disc):
    text = str(f.read().decode("utf-8")).split("\n")
    for line in text:
        code, name = line.split(";")
        check = Skill.objects.filter(code=code)
        if len(check) == 0:
            skill = Skill.objects.create(
                code=code,
                name=name,
                disc=disc
            )
            skill.save()


def loadpage(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            new_disc = Discipline(name=request.POST['discipline'])
            new_disc.save()
            handle_uploaded_file(request.FILES['file'], new_disc)
            return redirect('view')
        else:
            form = UploadFileForm()
    else:
        form = UploadFileForm()
    return render(request, 'upload.html', {'form': form})


def view_page(request):
    disciplines = Discipline.objects.all()

    return render(request, 'view.html', {'disciplines': disciplines})


def generate_page(request):
    if request.method == 'POST':
        form = GenerateTableForm(request.POST)
        if form.is_valid():
            disc = Discipline.objects.get(id=request.POST['discipline'])
            return generate_table(disc)
        else:
            form = GenerateTableForm()
    else:
        form = GenerateTableForm()
    return render(request, 'generate.html', {'form': form})
