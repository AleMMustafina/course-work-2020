# Generated by Django 3.0.1 on 2019-12-28 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generator', '0002_criteria_formofcontrol'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='formofcontrol',
            options={'verbose_name': 'Оценочное средство', 'verbose_name_plural': 'Оценочные средства'},
        ),
        migrations.AddField(
            model_name='skill',
            name='code',
            field=models.CharField(default='', max_length=128, verbose_name='Код'),
            preserve_default=False,
        ),
    ]
