from django import forms

from generator.models import Discipline


class UploadFileForm(forms.Form):
    # discipline = forms.ModelChoiceField(queryset=Discipline.objects.all(), empty_label="<Пусто>", label="Дисциплина")
    discipline = forms.CharField(empty_value="Пусто", label='Дисциплина')
    file = forms.FileField(label="Файл")


class GenerateTableForm(forms.Form):
    discipline = forms.ModelChoiceField(queryset=Discipline.objects.all(), empty_label="<Пусто>", label="Дисциплина")
