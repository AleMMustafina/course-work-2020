from django.contrib import admin

from generator.models import Discipline, Skill, FormOfControl, Criteria

admin.site.register(Discipline)
admin.site.register(Skill)
admin.site.register(Criteria)
admin.site.register(FormOfControl)
