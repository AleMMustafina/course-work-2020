from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('load', views.loadpage, name="load"),
    path('generate', views.generate_page, name="generate"),
    # url(r'^xls/$', views.export_xls, name='export_xls'),
    path('', views.view_page, name="view"),
]
