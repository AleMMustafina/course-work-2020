from django.db import models

CRITERIA_CHOICES = [
    ('A', 'Отлично'),
    ('B', 'Хорошо'),
    ('C', 'Удовл.'),
    ('D', 'Неуд.'),
]

FORM_OF_CONTROL_CHOICES = [
    ('E', 'Экзамен'),
    ('Q', 'Зачёт'),
    ('T', 'Контрольная работа'),
    ('H', 'Домашнее задание'),
]


class Discipline(models.Model):
    name = models.CharField(max_length=128, verbose_name="Навание")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Дисциплина"
        verbose_name_plural = "Дисциплины"


class Skill(models.Model):
    code = models.CharField(max_length=128, verbose_name="Код")
    name = models.CharField(max_length=128, verbose_name="Навание")
    disc = models.ForeignKey(Discipline, verbose_name="Дисциплина", on_delete=models.PROTECT, related_name="skills")

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = "Компетенция"
        verbose_name_plural = "Компетенции"


class Criteria(models.Model):
    type = models.CharField(max_length=1, choices=CRITERIA_CHOICES, verbose_name="Тип критерия")
    desc = models.TextField(verbose_name="Описание")
    skill = models.ForeignKey(Skill, verbose_name="Оцениваемая компетенция", on_delete=models.PROTECT, related_name="criteria")

    def __str__(self):
        return str(self.skill) + self.type

    class Meta:
        verbose_name = "Критерий оценки"
        verbose_name_plural = "Критерии оценки"


class FormOfControl(models.Model):
    type = models.CharField(max_length=1, choices=FORM_OF_CONTROL_CHOICES, verbose_name="Тип контроля")
    skills = models.ManyToManyField(Skill, verbose_name="Оцениваемые компетенции", related_name="foc")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = "Оценочное средство"
        verbose_name_plural = "Оценочные средства"
